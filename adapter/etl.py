from prefect import task, Flow
from prefect.schedules.schedules import IntervalSchedule
from prefect.engine.executors import DaskExecutor
from dask.distributed import Client
from sodapy import Socrata
import pandas as pd
import datetime as dt
import os

s3_bucket_path = os.getenv('S3_BASE_LOCATION')

@task(nout=2)
def extract():
    class Client:
        APP_TOKEN = "XNKZJVsscuBPRteOXyyM6G3yc"
        ROOT_URL = "data.cityofnewyork.us"
        END_POINT_ONE = "8b6c-7uty"
        END_POINT_TWO = "kybe-9iex"

    
        def request_school_info(self):
            sub_client = Socrata(self.ROOT_URL, app_token = self.APP_TOKEN)
            data = sub_client.get(self.END_POINT_ONE, content_type = "json")
            return data    
        
        def request_school_results(self):
            sub_client = Socrata(self.ROOT_URL, app_token = self.APP_TOKEN)
            data = sub_client.get(self.END_POINT_TWO, content_type = "json")
            return data 
            

    school_info = Client().request_school_info()
    school_results = Client().request_school_results()

    return school_info, school_results

@task
def create_transform_df(records):
    df = pd.DataFrame(records)
    columns = ['dbn','school_name','primary_address_line_1','city','postcode',
               'latitude','longitude','gradespan','subway','pct_stu_safe','graduation_rate',
               'attendance_rate','college_career_rate']
    transformed_df = df.loc[:, columns]
    return transformed_df

@task
def create_transform_df_two(records):
    df = pd.DataFrame(records)
    return df

@task
def merge(df_one,df_two):
    merged_df = pd.merge(left=df_one,right=df_two,on='dbn')
    return merged_df

@task
def load(transformed_df):
    current_date = dt.datetime.now().strftime('%m/%d/%Y')
    loaded_object = transformed_df.to_parquet(f"s3://tatumbucket/school_data={current_date}.parquet", engine='fastparquet')
    return loaded_object

def build_flow(schedule=None):
    with Flow("my_etl",schedule=schedule) as flow:
        data1,data2 = extract()
        df_1 = create_transform_df(data1)
        df_2 = create_transform_df_two(data2)
        merged_df = merge(df_1,df_2)
        tdata = load(merged_df)
    return flow



if __name__ == "__main__":
    schedule = IntervalSchedule(start_date=dt.datetime.now(),
                interval=dt.timedelta(seconds=10))
    flow = build_flow(schedule)
    flow.run()
